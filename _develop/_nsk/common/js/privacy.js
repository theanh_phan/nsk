//privacy
var privacy_url='http://www.nspc.nssmc.com/cookie/';

var html = '<div id="modal-view"></div>';
html += '<div id="privacy" style="display:none" class="privacy-container">';
html += '<div class="privacy-inner">';
html += '<p class="privacy-prefix">当社のウェブサイトは、利便性、品質維持・向上を目的に、<br>Cookieを使用しております。詳しくは';
html += '<a class="privacy-anchor" href="'+ privacy_url +'" target="_blank">クッキー使用について</a>をご覧ください。Cookieの利用に同意頂ける場合は、「同意する」ボタンを押してください。<br>';
html += '同意頂けない場合は、ブラウザを閉じて閲覧を中止してください。</p>';
html += '<a class="btn-close-main" href="javascript:;">';
html += '<span id="close-btn" class="privacy-btn-close flexiblebox">';
html += '<span class="privacy-btn-copy">同意する</span>';
html += '</span>';
html += '</a>';
html += '</div>';
html += '</div>';
var body = document.body;
body.insertAdjacentHTML('beforeend',html);

function privacy_policy_cookie(){
    var modal_view = document.getElementById("modal-view");
	var privacy_btn = document.getElementById("close-btn");
	var privacy_dialog = document.getElementById("privacy");
	var cookie_array = [];
	var cookie_split = document.cookie.split('; ');
	var privacy_expired_date =new Date();
	var privacy_expired_year = privacy_expired_date.getFullYear();
	var privacy_expired_month = privacy_expired_date.getMonth()+1;
	var privacy_expired_day = privacy_expired_date.getDate();
	var cookie_flg = false;
	var cookie_limit = 90;
	privacy_expired_date.setTime(privacy_expired_date.getTime() + cookie_limit*24*60*60*1000);
	var cookie_limit_date = privacy_expired_date.toGMTString();

	cookie_split.forEach(function(value) {
	    var content = value.split('=');
	    cookie_array.push(content);
	});

	if(cookie_array.length > 0){
		for(var i=0; i<cookie_array.length; i++ ){
			if (cookie_array[i].indexOf('_nspc_privacy_policy') == 0){
        		cookie_flg = true;
    		}
		}
	}

	if(cookie_flg){
	    if(modal_view){
            modal_view.style.display = "none"
        }
		privacy_dialog.style.display="none";
	}else{
	    if(modal_view){
            modal_view.style.display = "block"
        }
		privacy_dialog.style.display="block";
	}

	privacy_btn.onclick = function(){
		document.cookie = '_nspc_privacy_policy=' + "accept " + (privacy_expired_year + '/' + privacy_expired_month + '/' + privacy_expired_day) + ";expires=" + cookie_limit_date + ";path=/";
		if(modal_view){
            modal_view.style.display = "none"
        }
		privacy_dialog.style.display="none";
	}
}
privacy_policy_cookie();
