// JavaScript Document
$(function() {
  jQuery('img.svg').each(function() {
    var $img = jQuery(this);
    var imgID = $img.attr('id');
    var imgClass = $img.attr('class');
    var imgURL = $img.attr('src');
    jQuery.get(imgURL, function(data) {
      var $svg = jQuery(data).find('svg');
      if (typeof imgID !== 'undefined') {
        $svg = $svg.attr('id', imgID);
      }
      if (typeof imgClass !== 'undefined') {
        $svg = $svg.attr('class', imgClass + ' replaced-svg');
      }
      $svg = $svg.removeAttr('xmlns:a');
      if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
        $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
      }
      $img.replaceWith($svg);

    }, 'xml');

  });
});

$(document).ready(function(){
  $('.section-info .animation').each(function(){
        var POS = $(this).offset().top;
        var scroll = $(window).scrollTop();
        var windowHeight = $(window).height();

        if (scroll > POS - windowHeight){
            $(this).addClass('fadeAnimate');
        }else {
            $(this).removeClass('fadeAnimate');
        }
    });
});
$(window).on('load',function(){
    // fade-up
    $('.animation').each(function(){
        var POS = $(this).offset().top;
        var scroll = $(window).scrollTop();
        var windowHeight = $(window).height();

        if (scroll > POS - windowHeight){
            $(this).addClass('fadeAnimate');
        }else {
            $(this).removeClass('fadeAnimate');
        }
    });
    $(window).scroll(function (){
        $('.animation').each(function(){
            var POS = $(this).offset().top;
            var scroll = $(window).scrollTop();
            var windowHeight = $(window).height();

            if (scroll > POS - windowHeight){
                $(this).addClass('fadeAnimate');
            }else {
                $(this).removeClass('fadeAnimate');
            }
        });
    });
});

$(document).ready(function(){
    $('.pagetop').hide();
    $(window).scroll(function(){ 
        if ($(this).scrollTop() > 400) { 
            $('.pagetop').fadeIn();
        } else { 
            $('.pagetop').fadeOut();
        } 
    }); 
    $('.pagetop').click(function(){ 
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false; 
    });
});


var element = document.querySelector(".pagetop");

function Scroll () {
  
      var timeoutId ;

window.addEventListener( "scroll", function () {
  element.classList.add( "active" ) ;

  clearTimeout( timeoutId ) ;

  timeoutId = setTimeout( function () {
    element.classList.remove( "active" ) ;
  }, 500 ) ;
} ) ;
  }
window.addEventListener("scroll", Scroll);