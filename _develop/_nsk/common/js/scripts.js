$(function() {

    /* Common Script
    ========================================================= */

    /**** Change image hover ****/
    $('.changeHover').hover(function() {
        $(this, 'a').find('img').stop().fadeToggle();

    });

    /**** Open Menu Right ****/
    $(".menuOpen").click(function() {
        $(this).toggleClass("active");
        $('nav#main_menu').toggleClass("active");
        $('.menuCover').toggleClass("active");
        return false;
    });

    $('.menuCover').click(function() {
        $(this).removeClass("active");
        $('nav#main_menu').removeClass("active");
        $('.menuOpen').removeClass("active");
    });


    /**** Click anchor link ****/
    $('a[href^=#]').click(function() {
        var speed = 500;
        var href = $(this).attr("href");
        var target = $(href == "" ? 'html' : href);
        var position = target.offset().top - 70;
        $('html,body').animate({ scrollTop: position }, speed, 'swing');
        return false;
    });

    /**** Change imgae pc and sp ****/
    function imageSwitch() {
        var $elem = $('.switch');
        var sp = '_sp.';
        var pc = '_pc.';
        var replaceWidth = 768;

        var windowWidth = parseInt($(window).width());
        $elem.each(function() {
            var $this = $(this);
            if (windowWidth >= replaceWidth) {
                $this.attr('src', $this.attr('src').replace(sp, pc));
            } else {
                $this.attr('src', $this.attr('src').replace(pc, sp));
            }
        });
    }
    imageSwitch();

    var resizeTimer;
    $(window).on('resize', function() {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function() {
            imageSwitch();
        }, 200);
    });


    /**** Change background header in scroll ****/
    $(window).scroll(function() {
        st = parseInt($(this).scrollTop());
        $(".move").not(".on").each(function() {
            if ($(this).offset().top - ($(window).height() * 0.95) < st) {
                $(this).addClass("on");
            }
        });

        if (st > 61) {
            $("header").addClass("change");
        } else {
            $("header").removeClass("change");
        }
    });


    /* Top page Script
    ========================================================= */

    /**** Add poster video sp ****/
    $(window).on('load', function() {
        var windowWidth = parseInt($(window).width());
        if (windowWidth <= 640) {
            $('video').attr("poster", "common/img/poster_video_sp.png");
        } else {
            $('video').attr("poster", "common/img/poster_video_pc.png");
        }
    });

    /**** Text top in pc ****/
    $('.top_copy_txt.is_pc').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {

            $('.text_show01').each(function(index, el) {
                var groups = $(this).find('span');
                var tl = new TimelineMax();
                tl.staggerTo(groups, 0.01, { autoAlpha: 1 }, 0.05);
            }, 0);
            setTimeout(function() {
                $('.text_show02').each(function(index, el) {
                    var groups = $(this).find('span');
                    var tl = new TimelineMax();
                    tl.staggerTo(groups, 0.01, { autoAlpha: 1 }, 0.05);
                });
            }, 500);

            setTimeout(function() {
                $('.text_show03').each(function(index, el) {
                    var groups = $(this).find('span');
                    var tl = new TimelineMax();
                    tl.staggerTo(groups, 0.01, { autoAlpha: 1 }, 0.05);
                });
            }, 1500);

            setTimeout(function() {
                $('.text_show04').each(function(index, el) {
                    var groups = $(this).find('span');
                    var tl = new TimelineMax();
                    tl.staggerTo(groups, 0.01, { autoAlpha: 1 }, 0.05);
                });
            }, 2500);

            setTimeout(function() {
                $('.top_copy_img01').addClass('animated slideInUp');
            }, 4100);

            setTimeout(function() {
                $('.top_copy_img02').addClass('animated slideInUp');
            }, 4600);

            setTimeout(function() {
                $('.top_copy_img03').addClass('animated slideInUp');
            }, 5100);

            setTimeout(function() {
                $('.top_copy_img04').addClass('animated slideInUp');
            }, 5600);

        } else {
            $(this).off('inview');
        }
    });

    /**** Text top in pc ****/
    $('.top_copy_txt.is_sp').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {

            $('.text_show01').each(function(index, el) {
                var groups = $(this).find('span');
                var tl = new TimelineMax();
                tl.staggerTo(groups, 0.01, { autoAlpha: 1 }, 0.05);
            }, 0);
            setTimeout(function() {
                $('.text_show02').each(function(index, el) {
                    var groups = $(this).find('span');
                    var tl = new TimelineMax();
                    tl.staggerTo(groups, 0.01, { autoAlpha: 1 }, 0.05);
                });
            }, 200);

            setTimeout(function() {
                $('.text_show03').each(function(index, el) {
                    var groups = $(this).find('span');
                    var tl = new TimelineMax();
                    tl.staggerTo(groups, 0.01, { autoAlpha: 1 }, 0.05);
                });
            }, 600);

            setTimeout(function() {
                $('.text_show04').each(function(index, el) {
                    var groups = $(this).find('span');
                    var tl = new TimelineMax();
                    tl.staggerTo(groups, 0.01, { autoAlpha: 1 }, 0.05);
                });
            }, 800);

            setTimeout(function() {
                $('.text_show05').each(function(index, el) {
                    var groups = $(this).find('span');
                    var tl = new TimelineMax();
                    tl.staggerTo(groups, 0.01, { autoAlpha: 1 }, 0.05);
                });
            }, 1200);

            setTimeout(function() {
                $('.text_show06').each(function(index, el) {
                    var groups = $(this).find('span');
                    var tl = new TimelineMax();
                    tl.staggerTo(groups, 0.01, { autoAlpha: 1 }, 0.05);
                });
            }, 1400);

            setTimeout(function() {
                $('.text_show07').each(function(index, el) {
                    var groups = $(this).find('span');
                    var tl = new TimelineMax();
                    tl.staggerTo(groups, 0.01, { autoAlpha: 1 }, 0.05);
                });
            }, 1600);

            setTimeout(function() {
                $('.text_show08').each(function(index, el) {
                    var groups = $(this).find('span');
                    var tl = new TimelineMax();
                    tl.staggerTo(groups, 0.01, { autoAlpha: 1 }, 0.05);
                });
            }, 1800);

            setTimeout(function() {
                $('.top_copy_img01').addClass('animated slideInUp');
            }, 3000);

            setTimeout(function() {
                $('.top_copy_img02').addClass('animated slideInUp');
            }, 3500);

            setTimeout(function() {
                $('.top_copy_img03').addClass('animated slideInUp');
            }, 4000);

            setTimeout(function() {
                $('.top_copy_img04').addClass('animated slideInUp');
            }, 4500);

        } else {
            $(this).off('inview');
        }
    });

    $('.banner_item.item01').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function() {
                $('.banner_item.item01 .banner_item_img a').addClass('animated zoomIn');
            }, 0);

            setTimeout(function() {
                $('.banner_item.item01 .banner_item_link a').addClass('animated zoomIn');
            }, 1000);
        } else {
            $(this).off('inview');
        }
    });

    $('.banner_item.item02').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function() {
                $('.banner_item.item02 .banner_item_img a').addClass('animated zoomIn');
            }, 0);

            setTimeout(function() {
                $('.banner_item.item02 .banner_item_link a').addClass('animated zoomIn');
            }, 1000);
        } else {
            $(this).off('inview');
        }
    });

    $('.banner_item.item03').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function() {
                $('.banner_item.item03 .banner_item_img a').addClass('animated zoomIn');
            }, 0);

            setTimeout(function() {
                $('.banner_item.item03 .banner_item_link a').addClass('animated zoomIn');
            }, 1000);
        } else {
            $(this).off('inview');
        }
    });

    $('.banner_item.item04').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function() {
                $('.banner_item.item04 .banner_item_img a').addClass('animated zoomIn');
            }, 0);

            setTimeout(function() {
                $('.banner_item.item04 .banner_item_link a').addClass('animated zoomIn');
            }, 1000);
        } else {
            $(this).off('inview');
        }
    });

    $('.banner_item.item05').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function() {
                $('.banner_item.item05 .banner_item_img a').addClass('animated zoomIn');
            }, 0);

            setTimeout(function() {
                $('.banner_item.item05 .banner_item_link a').addClass('animated zoomIn');
            }, 1000);
        } else {
            $(this).off('inview');
        }
    });

    $('.banner_item.item06').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function() {
                $('.banner_item.item06 .banner_item_img a').addClass('animated zoomIn');
            }, 0);

            setTimeout(function() {
                $('.banner_item.item06 .banner_item_link a').addClass('animated zoomIn');
            }, 1000);
        } else {
            $(this).off('inview');
        }
    });


    /* Company page Script
    ========================================================= */
    $('.keyvisual_company').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function() {
                $('.keyvisual_company .keyvisual_txt01').addClass('animated slideInDown');
            }, 500);
            setTimeout(function() {
                $('.keyvisual_company .keyvisual_txt03').addClass('animated slideInUp');
            }, 1000);
            setTimeout(function() {
                $('.keyvisual_company .keyvisual_txt02').addClass('animated zoomIn');
            }, 1500);
            setTimeout(function() {
                $('.keyvisual_company .keyvisual_cup01').addClass('animated zoomIn');
            }, 2000);
            setTimeout(function() {
                $('.keyvisual_company .keyvisual_cup02').addClass('animated zoomIn');
            }, 2500);
        }
    });

    $('.company_item.item01').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function() {
                $('.company_item.item01 .txt01').addClass('animated zoomIn');
            }, 0);
            setTimeout(function() {
                $('.company_item.item01 .cup01').addClass('animated zoomIn');
            }, 500);
            setTimeout(function() {
                $('.company_item.item01 .cup02').addClass('animated zoomIn');
            }, 1000);
        }
    });

    $('.company_item.item02').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function() {
                $('.company_item.item02 .txt01').addClass('animated zoomIn');
            }, 0);
            setTimeout(function() {
                $('.company_item.item02 .txt02').addClass('animated zoomIn');
            }, 500);
            setTimeout(function() {
                $('.company_item.item02 .img01').addClass('animated zoomIn');
            }, 1000);
            setTimeout(function() {
                $('.company_item.item02 .img02').addClass('animated zoomIn');
            }, 1500);
            setTimeout(function() {
                $('.company_item.item02 .txt03').addClass('animated zoomIn');
            }, 2000);
            setTimeout(function() {
                $('.company_item.item02 .txt04').addClass('animated zoomIn');
            }, 2500);
        }
    });

    $('.company_item.item03').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function() {
                $('.company_item.item03 .txt01').addClass('animated zoomIn');
            }, 0);
            setTimeout(function() {
                $('.company_item.item03 .txt02').addClass('animated zoomIn');
            }, 500);
            setTimeout(function() {
                $('.company_item.item03 .img01').addClass('animated zoomIn');
            }, 1000);
        } else {
            $(this).off('inview');
        }
    });

    $('.company_item.item04').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function() {
                $('.company_item.item04 .img01').addClass('animated zoomIn');
            }, 0);
            setTimeout(function() {
                $('.company_item.item04 .txt01').addClass('animated zoomIn');
            }, 500);
            setTimeout(function() {
                $('.company_item.item04 .txt02').addClass('animated zoomIn');
            }, 1000);

        } else {
            $(this).off('inview');
        }
    });

    $('.company_item.item05').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function() {
                $('.company_item.item05 .img01').addClass('animated zoomIn');
            }, 0);
        } else {
            $(this).off('inview');
        }
    });

    $('.company_item.item06').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function() {
                $('.company_item.item06 .txt01').addClass('animated zoomIn');
            }, 0);
            setTimeout(function() {
                $('.company_item.item06 .txt02').addClass('animated zoomIn');
            }, 500);
            setTimeout(function() {
                $('.company_item.item06 .img01').addClass('animated zoomIn');
            }, 1000);
            setTimeout(function() {
                $('.company_item.item06 .img02').addClass('animated zoomIn');
            }, 1500);
            setTimeout(function() {
                $('.company_item.item06 .txt03').addClass('animated zoomIn');
            }, 2000);
            setTimeout(function() {
                $('.company_item.item06 .txt04').addClass('animated zoomIn');
            }, 2500);

        } else {
            $(this).off('inview');
        }
    });

    $('.company_item.item07').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function() {
                $('.company_item.item07 .txt01').addClass('animated zoomIn');
            }, 0);
            setTimeout(function() {
                $('.company_item.item07 .txt02').addClass('animated zoomIn');
            }, 500);
            setTimeout(function() {
                $('.company_item.item07 .img01').addClass('animated zoomIn');
            }, 1000);
            setTimeout(function() {
                $('.company_item.item07 .img02').addClass('animated zoomIn');
            }, 1500);
            setTimeout(function() {
                $('.company_item.item07 .txt03').addClass('animated zoomIn');
            }, 2000);
            setTimeout(function() {
                $('.company_item.item07 .txt04').addClass('animated zoomIn');
            }, 2500);

        } else {
            $(this).off('inview');
        }
    });

    $('.company_item.item08').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function() {
                $('.company_item.item08 .txt01').addClass('animated zoomIn');
            }, 0);
            setTimeout(function() {
                $('.company_item.item08 .txt02').addClass('animated zoomIn');
            }, 500);
            setTimeout(function() {
                $('.company_item.item08 .img01').addClass('animated zoomIn');
            }, 1000);
            setTimeout(function() {
                $('.company_item.item08 .img02').addClass('animated zoomIn');
            }, 1500);
            setTimeout(function() {
                $('.company_item.item08 .txt03').addClass('animated zoomIn');
            }, 2000);
            setTimeout(function() {
                $('.company_item.item08 .txt04').addClass('animated zoomIn');
            }, 2500);

        } else {
            $(this).off('inview');
        }
    });

    $('.company_item.item09').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function() {
                $('.company_item.item09 .img01').addClass('animated zoomIn');
            }, 0);
        } else {
            $(this).off('inview');
        }
    });

    $('.company_item.item10').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function() {
                $('.company_item.item10 .txt01').addClass('animated zoomIn');
            }, 0);
            setTimeout(function() {
                $('.company_item.item10 .txt02').addClass('animated zoomIn');
            }, 500);
            setTimeout(function() {
                $('.company_item.item10 .img02').addClass('animated zoomIn');
            }, 1000);

        } else {
            $(this).off('inview');
        }
    });

    $('.company_item.item11').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function() {
                $('.company_item.item11 .txt01').addClass('animated zoomIn');
            }, 0);
            setTimeout(function() {
                $('.company_item.item11 .txt02').addClass('animated zoomIn');
            }, 500);
            setTimeout(function() {
                $('.company_item.item11 .img02').addClass('animated zoomIn');
            }, 1000);

        } else {
            $(this).off('inview');
        }
    });

    $('.company_item.item12').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function() {
                $('.company_item.item12 .img01').addClass('animated zoomIn');
            }, 0);
            setTimeout(function() {
                $('.company_item.item12 .img02').addClass('animated zoomIn');
            }, 500);

        } else {
            $(this).off('inview');
        }
    });


    /* Message page Script
    ========================================================= */

    $('.page_message .keyvisual.is_pc').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function() {
                $('#ribbon').addClass('animated slideInDown');
            }, 0);
            setTimeout(function() {
                $('#txt01').addClass('animated zoomIn');
            }, 340);
            setTimeout(function() {
                $('#img01')
                .css({top: 300, opacity: 0, display: "block"})
                .animate({top: 246, opacity: 1}, 660, "");
            }, 450);
            setTimeout(function() {
                $('#img02')
                .css({top: 150, opacity: 0, display: "block"})
                .animate({top: 193, opacity: 1}, 660, "");
            }, 700);
            setTimeout(function() {
                $('#img03')
                .css({top: 150, opacity: 0, display: "block"})
                .animate({top: 204, opacity: 1}, 660, "");
            }, 700);
            setTimeout(function(){
                $("#img04")
                .css({top: 450, opacity: 0, display: "block"})
                .animate({top: 422, opacity: 1}, 660, "");
            }, 900);

            setTimeout(function(){
                $("#img05")
                .css({ width: 0, height: 0, top: 518, left: 345, display: "block"})
                .animate({ width: 497, height: 94, top: 476, left: 90,}, 760, "");
            }, 1000);
        } else {
            $(this).off('inview');
        }
    });

    $('.message_content01').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function(){
                $('.message_balloon01')
                    .css({width: 0, height: 0, top: 185.5, left: 421, display: "block"})
                    .animate({width: 718, height: 223, top: 74, left: 62}, 460, "");
            }, 1500);

            setTimeout(function(){
                $(".message_img01")
                    .css({width: 0, height: 0, top: 813.5, left: 430, display: "block"})
                    .animate({width: 839, height: 517, top: 555, left: 9}, 760, "");
            }, 2500);

            setTimeout(function(){
                $(".message_txt01")
                    .css({ top: 400, opacity: 0, display: "block"})
                    .animate({top: 320, opacity: 1,}, 800, "");
            }, 2000);

        } else {
            $(this).off('inview');
        }
    });

    $('.message_content02').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function(){
                $('.message_balloon02')
                    .css({width: 0, height: 0, top: 247.5, left: 345, display: "block"})
                    .animate({width: 690, height: 225, top: 135, left: 79}, 460, "");
            }, 0);

            setTimeout(function(){
                $(".message_img02")
                    .css({width: 0, height: 0, top: 907.5, left: 430, display: "block"})
                    .animate({width: 860, height: 539, top: 638, left: 0}, 460, "");
                }, 1200);

            setTimeout(function(){
                $(".message_txt02")
                    .css({ top: 400, opacity: 0, display: "block"})
                    .animate({top: 367, opacity: 1,}, 800, "");
            }, 450);

        } else {
            $(this).off('inview');
        }
    });


    $('.message_content03').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function(){
                $('.message_balloon03')
                    .css({width: 0, height: 0, top: 279, left: 423, display: "block"})
                    .animate({width: 718, height: 224, top: 167, left: 64}, 260, "");
            }, 0);

            setTimeout(function(){
                $(".message_img03")
                    .css({opacity: 0, top: 600, left: 0, right: 0, display: "block"})
                    .animate({opacity: 1, top: 695, left: 0, right: 0}, 260, "");
            }, 1200);

            setTimeout(function(){
                $(".message_txt03")
                    .css({ top: 500, opacity: 0, display: "block"})
                    .animate({top: 414, opacity: 1,}, 800, "");
            }, 450);

        } else {
            $(this).off('inview');
        }
    });


    /* Personality page Script
    ========================================================= */
    $('.personality_item.item01').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function() {
                $('.personality_item.item01').find('.item_bg').addClass('animated zoomIn');
                $('.personality_item.item01').find('.item_img').addClass('animated slideInUp');
                $('.personality_item.item01').find('.item_text').addClass('animated slideInDown');
            }, 200);
        }
    });

    $('.personality_item.item02').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function() {
                $('.personality_item.item02').find('.item_bg').addClass('animated zoomIn');
                $('.personality_item.item02').find('.item_img').addClass('animated slideInUp');
                $('.personality_item.item02').find('.item_text').addClass('animated slideInDown');
            }, 500);
        }
    });

    $('.personality_item.item03').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            setTimeout(function() {
                $('.personality_item.item03').find('.item_bg').addClass('animated zoomIn');
                $('.personality_item.item03').find('.item_img').addClass('animated slideInUp');
                $('.personality_item.item03').find('.item_text').addClass('animated slideInDown');
            }, 1000);
        }
    });


    /* Product page Script
    ========================================================= */
    $('.product_heading').on('inview', function(event, isInView) {
        if (isInView) {
            setTimeout(function() {
                $('.product_heading_txt01').addClass('animated zoomIn');
            }, 500);
            setTimeout(function() {
                $('.product_heading_txt02').addClass('animated zoomIn');
            }, 1000);
            setTimeout(function() {
                $('.product_heading_img01').addClass('animated zoomIn');
            }, 1300);
            setTimeout(function() {
                $('.product_heading_img02').addClass('animated zoomIn');
            }, 1300);
            setTimeout(function() {
                $('.product_heading_txt03').addClass('animated zoomIn');
            }, 1700);
        } else {
            $(this).off('inview');
        }
    });

    $('.product_heading_bottom').on('inview', function(event, isInView) {
        if (isInView) {
            setTimeout(function() {
                $('.product_heading_txt04').addClass('animated zoomIn');
            }, 500);
        } else {
            $(this).off('inview');
        }
    });

    /**** Product modal ****/
    $('.product_map button').on('click', function() {
        var href = $(this).text();
        $('#'+ href).show();
        $('.modal_content').addClass('animated zoomIn');
        $('body').css({'overflow': 'hidden'});
    });

    $('.modal .close').on('click', function() {
        $(this).parents('.modal').css({'display': 'none'});
        $('body').css({'overflow': 'auto'});
    });


    /* Technology page Script
    ========================================================= */

    $('.keyvisual_technology').on('inview', function(event, isInView) {
        if (isInView) {
            setTimeout(function() {
                $('.keyvisual_bg').addClass('animated zoomIn');
            }, 500);
            setTimeout(function() {
                $('.keyvisual_txt01_before, .keyvisual_txt01_after').addClass('animated zoomIn');
            }, 1000);
            setTimeout(function() {
                $('.keyvisual_txt01').addClass('animated zoomIn');
            }, 1300);
            setTimeout(function() {
                $('.keyvisual_txt02').addClass('animated zoomIn');
            }, 1600);
            setTimeout(function() {
                $('.keyvisual_txt03').addClass('animated zoomIn');
            }, 1900);
            setTimeout(function() {
                $('.keyvisual_txt04').addClass('animated zoomIn');
            }, 2200);
        } else {
            $(this).off('inview');
        }
    });

    /**** Technology acordion ****/
    $("#accordion .accordion_item_title").click(function(){
        var thisRoot = $(this).parents().filter('.accordion_item');
        if( !($(thisRoot).find('.accordion_content').size()) ) return;

        if( $(thisRoot).find('.accordion_content').is(':visible') ){
            $(thisRoot).find('.accordion_content').slideUp();
            $(thisRoot).removeClass('open');
        } else {
            $(thisRoot).find('.accordion_content').slideDown();
            $(thisRoot).addClass('open');
        }

    });

});


